import { ProSidebar, Menu, MenuItem, SubMenu } from "react-pro-sidebar";

const Sidebar = () => {
  return (
    <ProSidebar
      style={{ position: "fixed", backgroundColor: "#252525 !important" }}
      collapsed={false}
      width="240px"
    >
      <Menu iconShape="square">
        <MenuItem>Dashboard</MenuItem>
        <SubMenu title="Components">
          <MenuItem>Component 1</MenuItem>
          <MenuItem>Component 2</MenuItem>
        </SubMenu>
      </Menu>
    </ProSidebar>
  );
};

export default Sidebar;
