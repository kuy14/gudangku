import React, { useEffect } from "react";
import Sidebar from "./Sidebar";
import TopBar from "./TopBar";

const Layout = (props: { children: React.ReactNode }) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <React.Fragment>
      <div id="layout-wrapper">
        <TopBar />
        <Sidebar />
        <div style={{padding: "10px 10px 10px 280px"}}>{props.children}</div>
      </div>
    </React.Fragment>
  );
};

export default Layout;
