import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";

const TopBar = () => {
  return (
    <Navbar variant="dark" style={{height: "45px", backgroundColor: "#1c1c1c"}}>
      <Navbar.Brand href="#home" style={{width: "200px"}}>Navbar</Navbar.Brand>
      <Nav className="ml-auto">
        <Nav.Link href="#home">Home</Nav.Link>
        <Nav.Link href="#features">Features</Nav.Link>
        <Nav.Link href="#pricing">Pricing</Nav.Link>
      </Nav>
    </Navbar>
  );
};

export default TopBar;
